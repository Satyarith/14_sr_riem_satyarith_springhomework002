package com.example.homeworkii.controller;

import com.example.homeworkii.model.entity.Product;
import com.example.homeworkii.model.request.ProductRequest;
import com.example.homeworkii.model.respone.ApiRespone;
import com.example.homeworkii.service.ProductService;
import org.apache.ibatis.annotations.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class ProductController {
  private final ProductService productService;


  ProductController(ProductService productService) {
        this.productService = productService;
    }
    @GetMapping("/products")
    public List<Product> getAllCustomer(){
      return productService.getAllProduct();
    }

    @GetMapping("/produts/{id}")
    public ResponseEntity<?> getProductById(@PathVariable Integer id){
      Product product = productService.getProductById(id);
        ApiRespone<Product> respone = new ApiRespone<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Successful!",
                product
        );
        return ResponseEntity.ok(respone);
    }

    @PostMapping("/products")
    public ResponseEntity<?> insertProduct(@RequestBody Product product){
      return ResponseEntity.ok(new ApiRespone<Product>(
              LocalDateTime.now(),
              HttpStatus.OK,
              "Successful",
              productService.insertProduct(product)

      ));
    }
    @PutMapping("/products/{id}")
    public ResponseEntity<?> updateProduct(@PathVariable Integer id, @RequestBody ProductRequest productRequest) {
        productService.updateProductById(id, productRequest);
        return ResponseEntity.ok(new ApiRespone<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Successful",
                null
        ));
    }
      @DeleteMapping("/products/{id}")
              public ResponseEntity<?> deleteProduct(@PathVariable Integer id){
          productService.deleteProductById(id);
          return ResponseEntity.ok(new ApiRespone<>(
                  LocalDateTime.now(),
                  HttpStatus.OK,
                  "Successful Delete Customer",
                  null
          ));
        }

    }






















