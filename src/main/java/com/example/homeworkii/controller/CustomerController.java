package com.example.homeworkii.controller;


import com.example.homeworkii.model.request.CustomerRequest;
import com.example.homeworkii.model.respone.ApiRespone;
import com.example.homeworkii.model.entity.Customer;
import com.example.homeworkii.service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class CustomerController {

    private final CustomerService customerService;

    CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/customers")
    public List<Customer> getAllCustomer() {
        return customerService.getAllCustomer();

    }

    @GetMapping("/customers/{id}")
    public ResponseEntity<?> getCustomerById(@PathVariable Integer id) {
        Customer customer = customerService.getCustomerById(id);
        System.out.println(customer.getCustomer_id());
        ApiRespone<Customer> respone = new ApiRespone<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Successful",
                customer
        );
        return ResponseEntity.ok(respone);
    }


    @PostMapping("/customers")
    public ResponseEntity<?> insertCustomer(@RequestBody Customer customer) {

        return ResponseEntity.ok(new ApiRespone<Customer>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Successful",
                customerService.insertCustomer(customer)
        ));

    }

    @PutMapping("/customer/{id}")
    public ResponseEntity<?> updateCustomer(@PathVariable Integer id, @RequestBody CustomerRequest customerRequest) {
        customerService.updateCustomerById(id, customerRequest);
        return ResponseEntity.ok(new ApiRespone<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Successful",
                null
        ));

    }

    @DeleteMapping("/customers/{id}")

    public ResponseEntity<?> deleteCustomer(@PathVariable Integer id) {
        customerService.deleteCustomerById(id);
        return ResponseEntity.ok(new ApiRespone<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Successful Delete Customer",
                null
        ));

    }
}