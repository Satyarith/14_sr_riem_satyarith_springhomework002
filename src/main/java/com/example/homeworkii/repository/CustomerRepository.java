package com.example.homeworkii.repository;

import com.example.homeworkii.model.request.CustomerRequest;
import com.example.homeworkii.model.entity.Customer;
import org.apache.ibatis.annotations.*;

import java.util.List;


@Mapper
public interface CustomerRepository {

@Select("""
SELECT * FROM customer
""")
List<Customer> getAllCustomer();

//@Select("""
//        SELECT * FROM customer WHERE customer_id = 2;
//
//        """)
    @Select("SELECT * FROM customer WHERE customer_id = #{id}")
    Customer getCustomerById(Integer id);


    //user can see insert
    @Select("""
            INSERT INTO customer (customer_name, customer_address, customer_phone)
            VALUES (#{customer_name},#{customer_address},#{customer_phone})
            RETURNING *
            """)
    Customer insertCustomer(Customer customer);


    @Update("""
            UPDATE customer
                SET customer_name = #{customer.customer_name}, customer_address= #{customer.customer_address}, customer_phone =#{customer.customer_phone}
                WHERE customer_id=#{id};
            """)
    void updateCustomer(Integer id,@Param("customer") CustomerRequest customerRequest);
        @Delete("""
                DELETE FROM customer
                    WHERE customer_id=#{id};
                """)


    void deleteCustomerById(Integer id);



    //user can't see insert
//    @Insert("""
//            INSERT INTO customer (customer_name, customer_address, customer_phone)
//            VALUES (#{customer_name},#{customer_address},#{customer_phone})
//            RETURNING *
//            """)
//    void insertCustomer(Customer customer);



}
