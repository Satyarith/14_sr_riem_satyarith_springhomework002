package com.example.homeworkii.repository;


import com.example.homeworkii.model.entity.Product;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductRepository {

    @Select("""
            SELECT * FROM product
            """)
    List<Product> getAllProduct();


    @Select("SELECT * FROM product WHERE product_id=#{id}")
    Product getProductById(Integer id);

    @Select("""
            INSERT INTO product(prodcut_name, product_price)
            VALUES (#{product_name},#{product_price})
            RETURNING *
            """)
    Product insertProduct(Product product);

    @Update("""
            UPDATE product
            SET product_name = #{product.product_name}, product_price=#{product.product_price}
            WHERE product_id=#{id};
            """)
    <ProductRequest>

    void updateProduct(Integer id, @Param("product") ProductRequest productRequest);
    @Delete("""
            DELETE FROM product
            WHERE product_id=#{id};
            """)

    void deleteProductById(Integer id);

}
