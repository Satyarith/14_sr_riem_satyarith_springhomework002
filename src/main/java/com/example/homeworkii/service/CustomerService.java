package com.example.homeworkii.service;

import com.example.homeworkii.model.request.CustomerRequest;
import com.example.homeworkii.model.entity.Customer;

import java.util.List;

public interface CustomerService {

   List <Customer> getAllCustomer();

   Customer getCustomerById(Integer id);

   Customer insertCustomer(Customer customer);

   void updateCustomerById(Integer id, CustomerRequest customerRequest);

   void deleteCustomerById(Integer id);
}
