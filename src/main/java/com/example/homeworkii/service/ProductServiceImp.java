package com.example.homeworkii.service;
import com.example.homeworkii.model.entity.Product;
import com.example.homeworkii.model.request.ProductRequest;
import com.example.homeworkii.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProductServiceImp implements ProductService {
    private final ProductRepository productRepository;

    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    @Override
    public List<Product> getAllProduct() {

        return productRepository.getAllProduct();
    }

    @Override
    public Product getProductById(Integer id) {

        return productRepository.getProductById(id);
    }

    @Override
    public Product insertProduct(Product product) {

        return productRepository.insertProduct(product);
    }

    @Override
    public <ProductRequest> void updateProductById(Integer id, ProductRequest productRequest) {
        productRepository.updateProduct(id, productRequest);
    }

    @Override
    public void deleteProductById(Integer id) {
    productRepository.deleteProductById(id);
    }
}



