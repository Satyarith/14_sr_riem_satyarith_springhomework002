package com.example.homeworkii.service;
import com.example.homeworkii.model.request.CustomerRequest;
import com.example.homeworkii.model.entity.Customer;
import com.example.homeworkii.repository.CustomerRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImp implements CustomerService{
    private final CustomerRepository customerRepository;

    public CustomerServiceImp(CustomerRepository customerRepository) {

        this.customerRepository = customerRepository;
    }

//    @Autowired
//    public void setCustomerRepository(CustomerRepository customerRepository){
//        this.customerRepository = customerRepository;
//    }

    @Override
    public List<Customer> getAllCustomer() {

        return customerRepository.getAllCustomer();
    }

    @Override
    public Customer getCustomerById(Integer id) {
        return customerRepository.getCustomerById(id);
    }

    @Override
    public Customer insertCustomer(Customer customer) {

        return customerRepository.insertCustomer(customer);
    }

    @Override
    public void updateCustomerById(Integer id, CustomerRequest customerRequest) {
        customerRepository.updateCustomer(id, customerRequest);
    }

    @Override
    public void deleteCustomerById(Integer id) {
        customerRepository.deleteCustomerById(id);
    }
}
