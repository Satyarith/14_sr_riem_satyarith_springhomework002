package com.example.homeworkii.service;


import com.example.homeworkii.model.entity.Product;

import java.util.List;
public interface ProductService {

    List <Product> getAllProduct();
    Product getProductById(Integer id);
    Product insertProduct(Product product);
    <ProductRequest> void updateProductById(Integer id, ProductRequest productRequest);
    void deleteProductById(Integer id);


}
