package com.example.homeworkii;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeworkIiApplication {

    public static void main(String[] args) {

        SpringApplication.run(HomeworkIiApplication.class, args);
    }

}
