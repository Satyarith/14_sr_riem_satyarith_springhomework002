package com.example.homeworkii.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Product {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer product_id;
    private String product_name;
    private Integer product_price;


}
