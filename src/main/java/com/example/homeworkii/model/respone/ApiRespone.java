package com.example.homeworkii.model.respone;

import com.example.homeworkii.model.entity.Customer;
import com.example.homeworkii.model.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiRespone<T> {
    private LocalDateTime time;
    private HttpStatus status;
    private String message;
    private T payload;
}
