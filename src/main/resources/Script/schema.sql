CREATE TABLE product(
                           product_id SERIAL PRIMARY KEY,
                           product_name VARCHAR(30) NOT NULL ,
                           product_price INT NOT NULL
);

CREATE TABLE customer(
                            customer_id SERIAL PRIMARY KEY,
                            customer_name VARCHAR(30) NOT NULL,
                            customer_address VARCHAR(255) NOT NULL,
                            customer_phone VARCHAR(20) NOT NULL
);

CREATE TABLE invoice(
                           invoice_id SERIAL PRIMARY KEY,
                           invoice_date VARCHAR NOT NULL,
                           customer_id INT REFERENCES customer (customer_id)
);

CREATE TABLE invoice_detail(
                                  id SERIAL PRIMARY KEY,
                                  invoice_id INT REFERENCES invoice (invoice_id),
                                  product_id INT REFERENCES product (product_id)
);
